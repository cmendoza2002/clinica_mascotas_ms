package com.microservicio.mascotas.dominio;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Getter @Setter
public class Mascota {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre;
    private Date fechaNacimiento;
    private Long especieId;
    private String especieNombre;
    private Long propietarioId;
    private String propietarioNombre;
    private Sexo sexo;


}
