package com.microservicio.mascotas.dominio.repositorio;

import com.microservicio.mascotas.dominio.Mascota;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MascotaRepositorio extends JpaRepository<Mascota,Long> {
}
