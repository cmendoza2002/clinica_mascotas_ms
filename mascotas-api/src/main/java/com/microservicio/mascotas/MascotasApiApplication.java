package com.microservicio.mascotas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class MascotasApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MascotasApiApplication.class, args);
    }

}
