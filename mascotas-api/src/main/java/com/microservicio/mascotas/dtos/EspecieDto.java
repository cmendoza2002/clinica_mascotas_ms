package com.microservicio.mascotas.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EspecieDto {
    private int id;
    private String nombre;
}
