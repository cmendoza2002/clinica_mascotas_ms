package com.microservicio.mascotas.controladores;

import com.microservicio.mascotas.dominio.Mascota;
import com.microservicio.mascotas.dominio.repositorio.MascotaRepositorio;
import com.microservicio.mascotas.dtos.EspecieDto;
import com.microservicio.mascotas.dtos.PropietarioDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

@RestController
public class MascotaControlador {

    @Autowired
    private MascotaRepositorio mascotaRepositorio;

    @Autowired
    private RestTemplate restTemplate;


    @GetMapping("/mascota")
    public List<Mascota> obtenerTodas() {
        return mascotaRepositorio.findAll();
    }

    @GetMapping("/mascota/{id}")
    public Mascota obtenerPorId(@PathVariable("id") Long id) {
        Optional<Mascota> optionalMascota = mascotaRepositorio.findById(id);
        if(!optionalMascota.isPresent()) return null;
        return optionalMascota.get();
    }

    @PostMapping("/mascota")
    public Mascota agregar(@RequestBody Mascota mascota) {

        //RestTemplate restTemplate = new RestTemplate();

        //Sin balanceo ni descubrimiento
        //PropietarioDto propietarioDto = restTemplate.getForObject("http://localhost:8082/propietario/" + mascota.getPropietarioId(), PropietarioDto.class);
        //EspecieDto especieDto = restTemplate.getForObject("http://localhost:8081/especie/" + mascota.getEspecieId(), EspecieDto.class);


        //Con balanceo y descubrimiento
        PropietarioDto propietarioDto = restTemplate.getForObject("http://propietarios-api/propietario/" + mascota.getPropietarioId(), PropietarioDto.class);
        EspecieDto especieDto = restTemplate.getForObject("http://especie-api/especie/" + mascota.getEspecieId(), EspecieDto.class);

        if(propietarioDto==null || especieDto == null)
        {
            return null;
        }


        mascota.setEspecieNombre(especieDto.getNombre());
        mascota.setPropietarioNombre(propietarioDto.getNombre());


        Mascota entidad = mascotaRepositorio.save(mascota);
        return entidad;
    }

    @DeleteMapping("/mascota/{id}")
    public void eliminar(@RequestParam("id") Long id) {
        this.mascotaRepositorio.deleteById(id);
    }

    @PutMapping("/mascota")
    public void actualizar(@RequestBody Mascota especie) {
        this.mascotaRepositorio.save(especie);
    }

}






