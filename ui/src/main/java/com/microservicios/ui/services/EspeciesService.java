package com.microservicios.ui.services;

import com.microservicios.ui.models.Especie;
import com.microservicios.ui.util.ServiceBase;

public interface EspeciesService extends ServiceBase<Especie> {
}
