package com.microservicios.ui.services;

import com.microservicios.ui.config.Parametros;
import com.microservicios.ui.models.Especie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class EspeciesServiceImp implements EspeciesService {
    RestTemplate restTemplate = new RestTemplate();

    @Autowired
    Parametros parametros;


    @Override
    public void delete(Especie entity) {

        String url = parametros.getEspeciesUrl()+"/especie/{id}";
        Map< String, String > params = new HashMap< String, String >();
        params.put("id", "" + entity.getId());
        restTemplate.delete(url, params);
    }

    @Override
    public void save(Especie entity) {
        String url = parametros.getEspeciesUrl()+"/especie";
        Especie e = restTemplate.postForObject(url,entity,Especie.class);

    }

    @Override
    public Especie findOne(Long id) {
        return null;
    }

    @Override
    public List<Especie> findAll() {

        String url = parametros.getEspeciesUrl()+"/especie";
        ResponseEntity<Especie[]> response =
                restTemplate.getForEntity(
                        url,
                        Especie[].class);
        Especie[] employees = response.getBody();
        List<Especie> lista = Arrays.asList(employees);
        return lista;
    }

    @Override
    public List<Especie> findByString(String filter) {
        return findAll();
    }
}
