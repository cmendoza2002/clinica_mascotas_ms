package com.microservicios.ui.models;

import com.microservicios.ui.util.BaseEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Especie extends BaseEntity {
    private String nombre;
}
