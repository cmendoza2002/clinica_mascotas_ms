package com.microservicios.ui.views.forms;

import com.microservicios.ui.models.Especie;
import com.microservicios.ui.services.EspeciesService;
import com.microservicios.ui.util.BaseFormEntity;
import com.microservicios.ui.util.ServiceBase;
import com.vaadin.data.converter.StringToLongConverter;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Component;
import com.vaadin.ui.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.layouts.MFormLayout;

@ViewScope
@SpringComponent
public class EspecieForm extends BaseFormEntity<Especie> {

    @Autowired
    private EspeciesService service;

    private TextField id = new MTextField("Codigo:").withWidth("100%");
    private TextField nombre = new MTextField("Nombre:").withWidth("100%");

    public EspecieForm() {
        super(Especie.class);
        id.setEnabled(false);
        setModalWindowTitle("Especie");
        setModalWidth("50%");
        setModalHeight("-1px");
    }

    @Override
    protected void bind() {

        getBinder().forField(id)
                .withConverter(new StringToLongConverter("Solo numeros")).bind("id");

        super.bind();
    }

    @Override
    public ServiceBase<Especie> getService() {
        return service;
    }

    @Override
    protected Component createContent() {
        return getDefaultLayoutContent(new MFormLayout(
                id,
                nombre
        ).withWidth("100%"));
    }
}
