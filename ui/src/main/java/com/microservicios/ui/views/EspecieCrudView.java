package com.microservicios.ui.views;

import com.microservicios.ui.models.Especie;
import com.microservicios.ui.services.EspeciesService;
import com.microservicios.ui.util.BaseCrudView;
import com.microservicios.ui.util.Sections;
import com.microservicios.ui.util.ServiceBase;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.sidebar.annotation.FontAwesomeIcon;
import org.vaadin.spring.sidebar.annotation.SideBarItem;
import org.vaadin.viritin.grid.MGrid;


@SideBarItem(sectionId = Sections.CATALOGOS,caption = "Especie",order = 11)
@SpringView(name = EspecieCrudView.VIEW_NAME)
@FontAwesomeIcon(FontAwesome.BANK)
public class EspecieCrudView extends BaseCrudView<Especie> {

    public static final String VIEW_NAME = "especieCrudView";


    @Autowired
    private EspeciesService service;

    @Override
    public Especie getNew() {
        return new Especie();
    }

    @Override
    public ServiceBase<Especie> getService() {
        return service;
    }

    @Override
    public void listEntry(String nameFilter) {
            String filtro = "%"+nameFilter+"%";
            this.grid.setRows(service.findByString(filtro));
        adjustActionButtonState();
    }

    @Override
    public MGrid<Especie> getGrid() {

        if(grid ==null)
        {
            grid = new MGrid<>(Especie.class)
                    .withProperties("id","nombre")
                    .withColumnHeaders("id","Nombre");
        }

        return grid;
    }
}
