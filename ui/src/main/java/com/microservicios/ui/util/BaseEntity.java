package com.microservicios.ui.util;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class BaseEntity {

    private Long id = 0L;
    private boolean activo=true;

}
