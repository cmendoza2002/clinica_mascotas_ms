package com.microservicios.ui.util;

public class EntityModifiedEvent<E extends BaseEntity> {

    private final E entity;

    public EntityModifiedEvent(E e) {
        this.entity = e;
    }

    public E getEntity() {
        return entity;
    }
}
