package com.microservicios.ui.util;

import com.microservicios.ui.util.BaseEntity;

import java.util.List;

public interface ServiceBase<T extends BaseEntity> {
    void delete(T entity);
    void save(T entity);
    T findOne(Long id);
    List<T> findAll();
    List<T> findByString(String filter);
}
