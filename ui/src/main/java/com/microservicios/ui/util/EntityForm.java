package com.microservicios.ui.util;

import com.vaadin.ui.Window;

public interface EntityForm<E extends BaseEntity> {

    void setEntity(E entity);
    Window openInModalPopup();
    void closePopup();
    Window getPopup();
}
