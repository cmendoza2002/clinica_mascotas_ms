package com.microservicios.ui.util;

import java.io.Serializable;

public interface EditEndListener<T> extends Serializable {

    public void editEnd(T item);

}