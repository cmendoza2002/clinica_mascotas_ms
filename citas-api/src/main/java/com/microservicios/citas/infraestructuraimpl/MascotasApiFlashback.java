package com.microservicios.citas.infraestructuraimpl;

import com.microservicios.citas.dominio.infraestructura.MascotaApiClient;
import com.microservicios.citas.dtos.MascotaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

@Component
public class MascotasApiFlashback implements MascotaApiClient {


    @Autowired
    CacheManager cacheManager;



    @Override
    public MascotaDto obtenerPorId(Long id) {

        Cache.ValueWrapper w = cacheManager.getCache("mascotas").get(id);
        if (w != null) {
            return (MascotaDto) w.get();
        } else {
            MascotaDto mascotaDto = new MascotaDto();
            mascotaDto.setId(100L);
            mascotaDto.setNombre("Desconocido");
            return mascotaDto;
        }


        /*MascotaDto mascotaDto = new MascotaDto();
        mascotaDto.setId(100L);
        mascotaDto.setNombre("Desconocido");
        return mascotaDto;*/
    }
}
