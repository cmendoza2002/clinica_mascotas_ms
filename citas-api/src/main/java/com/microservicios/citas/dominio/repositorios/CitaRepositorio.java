package com.microservicios.citas.dominio.repositorios;

import com.microservicios.citas.dominio.Cita;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CitaRepositorio extends JpaRepository<Cita,Long> {
}
