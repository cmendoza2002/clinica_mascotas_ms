package com.microservicios.citas.dominio.infraestructura;


import com.microservicios.citas.dtos.MascotaDto;
import com.microservicios.citas.infraestructuraimpl.MascotasApiFlashback;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "mascotas-api", fallback = MascotasApiFlashback.class)
public interface MascotaApiClient {

    @CachePut("mascotas")
    @GetMapping("/mascota/{id}")
    MascotaDto obtenerPorId(@PathVariable("id") Long id);
}
