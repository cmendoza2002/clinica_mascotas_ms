package com.microservicios.citas.controladores;


import com.microservicios.citas.dominio.Cita;
import com.microservicios.citas.dominio.infraestructura.MascotaApiClient;
import com.microservicios.citas.dominio.repositorios.CitaRepositorio;
import com.microservicios.citas.dtos.MascotaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class CitasControlador {

    @Autowired
    private CitaRepositorio citaRepositorio;

    @Autowired
    private MascotaApiClient mascotaApiClient;


    @GetMapping("/cita")
    public List<Cita> obtenerTodas() {
        return citaRepositorio.findAll();
    }

    @GetMapping("/cita/{id}")
    public Cita obtenerPorId(@RequestParam("id") Long id) {

        Optional<Cita> optionalCita = citaRepositorio.findById(id);
        if(!optionalCita.isPresent()) return null;

        return optionalCita.get();
    }

    @PostMapping("/citas")
    public Cita agregar(@RequestBody Cita cita) {

        MascotaDto mascotaDto = this.mascotaApiClient.obtenerPorId(cita.getMascotaId());

        if(mascotaDto == null) return null;

        cita.setNombreMascota(mascotaDto.getNombre());


        Cita entidad = citaRepositorio.save(cita);
        return entidad;
    }

    @DeleteMapping("/cita/{id}")
    public void eliminar(@RequestParam("id") Long id) {
        this.citaRepositorio.deleteById(id);
    }

    @PutMapping("/citas")
    public void actualizar(@RequestBody Cita especie) {
        this.citaRepositorio.save(especie);
    }
}
