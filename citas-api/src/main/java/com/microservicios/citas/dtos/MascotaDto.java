package com.microservicios.citas.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class MascotaDto {
    private Long id;
    private String nombre;
}
