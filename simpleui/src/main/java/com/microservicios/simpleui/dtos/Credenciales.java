package com.microservicios.simpleui.dtos;


import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Credenciales {

    private String access_token;
    private String token_type;
    private  String scope;
}
