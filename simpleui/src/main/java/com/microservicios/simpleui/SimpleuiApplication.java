package com.microservicios.simpleui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleuiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleuiApplication.class, args);
    }

}
