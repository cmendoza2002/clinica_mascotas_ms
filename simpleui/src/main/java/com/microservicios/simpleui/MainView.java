package com.microservicios.simpleui;

import com.microservicios.simpleui.dtos.Credenciales;
import com.microservicios.simpleui.dtos.EspecieDto;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.Collections;
import java.util.List;


@Route("")
public class MainView extends VerticalLayout {

    private RestTemplate restTemplate = new RestTemplate();

    private Grid<EspecieDto> especieDtoGrid = new Grid<>();

    public MainView()
    {


        ResponseEntity<EspecieDto[]> especieDtos = this.restTemplate.getForEntity("http://localhost:9999/api/especies/especie?access_token=4337db00-1e08-4d8c-875b-f4daabb053b3",
                EspecieDto[].class);

        especieDtoGrid.addColumn(EspecieDto::getId).setHeader("id");
        especieDtoGrid.addColumn(EspecieDto::getNombre).setHeader("nombre");
        add(especieDtoGrid);
        especieDtoGrid.setItems(especieDtos.getBody());

        Button button = new Button();
        button.addClickListener(buttonClickEvent -> {
            getToken();
        });
        add(button);
    }


    public String getToken()
    {
        String url= "http://localhost:8989/oauth/token?grant_type=client_credentials";


        ResponseEntity<Credenciales> responseEntity = restTemplate.exchange
                (url, HttpMethod.GET, new HttpEntity<String>(createHeaders("micro", "micro.123")) ,Credenciales.class );


        Notification.show(responseEntity.getBody().getAccess_token());
        return responseEntity.getBody().getAccess_token();
    }


    HttpHeaders createHeaders(String username, String password){
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                    auth.getBytes(Charset.forName("US-ASCII")) );
            String authHeader = "Basic " + new String( encodedAuth );
            set( "Authorization", authHeader );
        }};
    }

}
