package com.microservicios.especie.dominio.repositorio;

import com.microservicios.especie.dominio.Especie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EspecieRepositorio extends JpaRepository<Especie, Long> {
}
