package com.microservicios.especie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class EspecieApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(EspecieApiApplication.class, args);
    }

}
