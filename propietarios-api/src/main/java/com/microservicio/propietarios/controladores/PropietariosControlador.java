package com.microservicio.propietarios.controladores;

import com.microservicio.propietarios.dominio.Propietario;
import com.microservicio.propietarios.dominio.infraestructura.PropietarioRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class PropietariosControlador {

    @Autowired
    private PropietarioRepositorio propietarioRepositorio;


    @GetMapping("/propietario")
    public List<Propietario> obtenerTodas() {
        return propietarioRepositorio.findAll();
    }

    @GetMapping("/propietario/{id}")
    public Propietario obtenerPorId(@PathVariable("id") Long id) {

        Optional<Propietario> optionalPropietario = propietarioRepositorio.findById(id);
        return  optionalPropietario.isPresent()?optionalPropietario.get():null;
    }

    @PostMapping("/propietario")
    public Propietario agregar(@RequestBody Propietario especie) {
        Propietario propietario = propietarioRepositorio.save(especie);
        return propietario;
    }

    @DeleteMapping("/propietario/{id}")
    public void eliminar(@PathVariable("id") Long id) {
        this.propietarioRepositorio.deleteById(id);
    }

    @PutMapping("/propietario")
    public void actualizar(@RequestBody Propietario propietario) {
        this.propietarioRepositorio.save(propietario);
    }


}
