package com.microservicio.propietarios.dominio.infraestructura;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.microservicio.propietarios.dominio.Propietario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PropietarioRepositorio extends JpaRepository<Propietario, Long> {
}
